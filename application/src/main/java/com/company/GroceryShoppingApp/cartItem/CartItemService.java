package com.company.GroceryShoppingApp.cartItem;

import com.company.GroceryShoppingApp.shoppingHistory.ShoppingHistory;
import com.company.GroceryShoppingApp.shoppingHistory.ShoppingHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartItemService {
    private final CartItemRepository cartItemRepository;
    private final ShoppingHistoryRepository shoppingHistoryRepository;

    @Autowired
    public CartItemService(CartItemRepository cartItemRepository, ShoppingHistoryRepository shoppingHistoryRepository) {
        this.cartItemRepository = cartItemRepository;
        this.shoppingHistoryRepository = shoppingHistoryRepository;
    }

    public List<CartItem> findCartItemsByShoppingHistoryId(Long id) {
        return cartItemRepository.findByShoppingHistoryId(id);
    }

    public void addSingleCartItem(CartItem cartItem) {
        if (shoppingHistoryRepository.findById(cartItem.getShoppingHistory().getId()).isPresent()){
            cartItemRepository.save(cartItem);
        }



    }
}