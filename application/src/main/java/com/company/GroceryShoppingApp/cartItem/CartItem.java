package com.company.GroceryShoppingApp.cartItem;

import com.company.GroceryShoppingApp.product.Product;
import com.company.GroceryShoppingApp.shoppingHistory.ShoppingHistory;
import com.company.GroceryShoppingApp.user.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;


@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity(name="cart_items")
@Table(name="cart_items")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CartItem {
    @Id
    @SequenceGenerator(
            name = "cart_item_sequence",
            sequenceName = "cart_item_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "cart_item_sequence"
    )
    private Long id;

    private Integer quantity;

    //unidirectional
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="product_id",referencedColumnName = "id")
    @JsonIgnore
    private Product product;


    //unidirectional, relationship at "many"(cart_item entity) side
    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name="shopping_history_id",referencedColumnName = "id")
    @JsonIgnore
    private ShoppingHistory shoppingHistory;


    public CartItem(Integer quantity, Product product,ShoppingHistory shoppingHistory) {
        this.quantity = quantity;
        this.product = product;
        this.shoppingHistory = shoppingHistory;
    }



    @Override
    public String toString() {
        return "{" +
                "\"quantity\":" + quantity +
                ", \"product\":"  + product.getId().toString() +
        "}";
    }
}
