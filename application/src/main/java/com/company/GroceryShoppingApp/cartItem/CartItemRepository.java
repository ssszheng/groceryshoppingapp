package com.company.GroceryShoppingApp.cartItem;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem,Long> {

    @Query("SELECT c FROM cart_items c WHERE c.shoppingHistory.id = ?1")
    List<CartItem> findByShoppingHistoryId(Long id);

}
