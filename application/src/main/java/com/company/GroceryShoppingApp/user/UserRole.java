package com.company.GroceryShoppingApp.user;

public enum UserRole {
    CUSTOMER,
    ADMIN
}
