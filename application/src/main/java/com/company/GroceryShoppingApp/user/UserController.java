package com.company.GroceryShoppingApp.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//API layer
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path="users/")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

   //array standpoint return all users to the client
    @GetMapping("all")
    public List<User> getUsers(){
        return userService.getUsers();
    }


    //update the existed user's information
    @PutMapping("{userid}")
    public  User updateUser(@PathVariable("userid") Long userid, @RequestBody User user ){
        return userService.updateUser(userid,user);
    }

    @DeleteMapping("{userid}")
    public User deleteUser(@PathVariable("userid") Long userid){
        return userService.deleteById(userid);
    }

}


