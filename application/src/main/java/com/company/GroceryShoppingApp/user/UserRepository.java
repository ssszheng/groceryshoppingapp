package com.company.GroceryShoppingApp.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

//data access layer
@Repository
@Transactional(readOnly = true)
public interface UserRepository
        extends JpaRepository<User,Long> {
    // this can transform to SQL
    //or use jpl query @Query
    @Query("SELECT u FROM users u WHERE u.email =?1")
    Optional<User> findByEmail(String email);

    @Transactional
    @Modifying
    @Query("UPDATE users u " +
            "SET u.enabled = TRUE WHERE u.email = ?1")
    int enableUser(String email);

}

