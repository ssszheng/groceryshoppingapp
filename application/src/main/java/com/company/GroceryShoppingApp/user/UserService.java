package com.company.GroceryShoppingApp.user;

import com.company.GroceryShoppingApp.registration.token.ConfirmationToken;
import com.company.GroceryShoppingApp.registration.token.ConfirmationTokenService;
import com.company.GroceryShoppingApp.review.Review;
import com.company.GroceryShoppingApp.review.ReviewRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

//service layer for business logic

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final ReviewRepository reviewRepository;
    private final static String USER_NOT_FOUND_MSG ="User with email %s not found";
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ConfirmationTokenService confirmationTokenService;

    //dependency injection, no need as AllArgsConstructor covered this
//    @Autowired
//    public UserService(UserRepository userRepository, ReviewRepository reviewRepository) {
//        this.userRepository = userRepository;
//        this.reviewRepository =reviewRepository;
//    }

    public List<User> getUsers() {
        return userRepository.findAll();

    }
    //create a token,
    // an email will be sent with the token link
    //user click the token link(post request)to enable the account
    public String signUpUser(User user) {
        //check if email exists
       boolean userExists = userRepository.findByEmail(user.getEmail()).isPresent();
       if (userExists) {throw new IllegalStateException("email existed");}
       String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
       user.setPassword(encodedPassword);
       userRepository.save(user);
       //create and send confirmation token
        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(20),
                user);
        confirmationTokenService.saveConfirmationToken(confirmationToken);
        return token;

    }


    public User getUserById(Long id){
        return userRepository.findById(id).orElseThrow(()->
                new UserNotFoundException(id));
    }
//    public User getUserByEmail(String email){
//        Optional<User> userOptional = userRepository.
//                findByEmail(email);
//        if(userOptional.isPresent()){
//            throw new IllegalStateException("email existed");
//        }
//        return userOptional.get();
//    }


    public User deleteById(Long userId){
        //first, find if the user exist
        User user = getUserById(userId);
        //second, need to delete the review accordingly
        removeReviewsFromUser(userId);
        userRepository.delete(user);
        return user;
    }
    public User updateUser(Long id, User user){
        User userToEdit = getUserById(id);
        userToEdit.setEmail(user.getEmail());
        userToEdit.setPassword(user.getPassword());
        return userToEdit;
    }
    public User addReviewToUser(Long userId,Long reviewId){
        User user = getUserById(userId);
        Review review =reviewRepository.getById(reviewId);
        user.addReview(review);
        return user;
    }
    public User removeReviewsFromUser(Long userId){
        //find if user exists
        User user = getUserById(userId);
        //get a list of review id
        List<Long> reviewsId = getReviewsIdByUserid(userId);
        for (Long rId : reviewsId) {
            //find review by id
            Optional<Review> review = reviewRepository.findById(rId);
            //user remove review
            review.ifPresent( r -> {
                user.removeReview(r);
            });
            //remove review in the review entity as well
            reviewRepository.deleteById(rId);
        }
        return user;
    }
    public List<Long> getReviewsIdByUserid(Long userId){
        User user = getUserById(userId);
        return reviewRepository.getReviewIdByUserId(userId);
    }
    public boolean checkLogin(String email, String password){
        Optional<User> optionalUser = userRepository.findByEmail(email);
        System.out.println("============"+optionalUser.get().getPassword());

        if (optionalUser.isPresent()
                && optionalUser.get().getPassword().equals(password)){
            return true;
        }return false;
    }

    @Override
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
        return userRepository.findByEmail(email)
                .orElseThrow(() ->
                        new UsernameNotFoundException(String.format(USER_NOT_FOUND_MSG,email)));

    }


    public int enableUser(String email) {
        return userRepository.enableUser(email);

    }

    public Optional<User> getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
