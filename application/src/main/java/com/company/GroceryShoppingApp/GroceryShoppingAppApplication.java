package com.company.GroceryShoppingApp;

import com.company.GroceryShoppingApp.cartItem.CartItem;
import com.company.GroceryShoppingApp.cartItem.CartItemRepository;
import com.company.GroceryShoppingApp.product.Product;
import com.company.GroceryShoppingApp.product.ProductRepository;
import com.company.GroceryShoppingApp.review.Review;
import com.company.GroceryShoppingApp.review.ReviewRepository;
import com.company.GroceryShoppingApp.shoppingHistory.ShoppingHistory;
import com.company.GroceryShoppingApp.shoppingHistory.ShoppingHistoryRepository;
import com.company.GroceryShoppingApp.user.User;
import com.company.GroceryShoppingApp.user.UserRepository;
import com.company.GroceryShoppingApp.user.UserRole;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class GroceryShoppingAppApplication  {

	public static void main(String[] args) {
		ConfigurableApplicationContext configurableApplicationContext =
				SpringApplication.run(GroceryShoppingAppApplication.class, args);

		// user repository offers db operations for Entities

		UserRepository userRepository = configurableApplicationContext.getBean(UserRepository.class);
		ProductRepository productRepository =configurableApplicationContext.getBean(ProductRepository.class);
        ReviewRepository reviewRepository = configurableApplicationContext.getBean(ReviewRepository.class);
		CartItemRepository cartItemRepository = configurableApplicationContext.getBean(CartItemRepository.class);
		ShoppingHistoryRepository shoppingHistoryRepository = configurableApplicationContext.getBean(ShoppingHistoryRepository.class);


//		User debbi= new User("Debbi","123@gamil.com","123",UserRole.CUSTOMER,false,true);
		User debbi= new User("Debbi","admin",new BCryptPasswordEncoder().encode("abc"),UserRole.ADMIN,false,true);
		User tom = new User("Tom","tom@gamil.com","password");

		Product apple = new Product("Apple",(float)3.40,"Fruit","Gala");
		Product milk = new Product("Milk",(float)5.22,"Dairy","Anchor");
		Product yogurt = new Product("yogurt",(float)7.80,"Dairy","Anchor");


		Review review1 = new Review(debbi,(float)4.5,"Good taste",apple);
		Review review2 = new Review(debbi,(float)5.0,"Good milk",milk);
		Review review3 = new Review(debbi,(float)3.0,"Good yogurt",yogurt);


//
		List<Review> reviewList = Arrays.asList(review1,review2);


		LocalDateTime dateTime1 = LocalDateTime.now();
		LocalDateTime dateTime2 = LocalDateTime.of(2010,10,1,19,25);

		ShoppingHistory shoppingHistory1= new ShoppingHistory(dateTime1,debbi);
		ShoppingHistory shoppingHistory2= new ShoppingHistory(dateTime2,debbi);

		CartItem cartItem1 = new CartItem(2,apple,shoppingHistory1);
		CartItem cartItem2= new CartItem(1,milk,shoppingHistory1);
		CartItem cartItem3= new CartItem(3,yogurt,shoppingHistory2);
		CartItem cartItem4 = new CartItem(2,apple,shoppingHistory2);



		List<CartItem> cartList1 = Arrays.asList(cartItem1,cartItem2); //2 x apple(1),1 x milk(2)
		List<CartItem> cartList2 = Arrays.asList(cartItem3,cartItem4);//3 x yogurt(3),2 x apple(1)



		System.out.println(review1);
		debbi.setReviews(reviewList);
//		System.out.println("-------------------------"+ shoppingHistory1.getCartItems().get(1).getProduct());

		productRepository.save(apple);
		productRepository.save(milk);
		productRepository.save(yogurt);

		userRepository.save(debbi);

		reviewRepository.save(review1);
		reviewRepository.save(review2);
		reviewRepository.save(review3);

		shoppingHistoryRepository.save(shoppingHistory1);
		shoppingHistoryRepository.save(shoppingHistory2);

		cartItemRepository.save(cartItem1);
		cartItemRepository.save(cartItem2);
		cartItemRepository.save(cartItem3);
		cartItemRepository.save(cartItem4);




		System.out.println("===============");
//

//		userRepository.save(tom);
//		userRepository.save(new User("Jessica","Jessica@gamil.com","password"));
//		this.productRepository.save(new Product("Apple",(float)4.00,"Fruit","Imported"));


	}
}
