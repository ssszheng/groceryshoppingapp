package com.company.GroceryShoppingApp.shoppingHistory;

import com.company.GroceryShoppingApp.cartItem.CartItem;
import com.company.GroceryShoppingApp.product.Product;
import com.company.GroceryShoppingApp.user.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity(name="shopping_histories")
@Table(name="shopping_histories")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ShoppingHistory {
    @Id
    @SequenceGenerator(
            name = "shopping_history_sequence",
            sequenceName = "shopping_history_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "shopping_history_sequence"
    )
    private Long id;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateTime;

    //unidirectional,relationship at shopping_history entity side
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id",referencedColumnName = "id")
    @JsonIgnore
    private User user;

//    //unidirectional, relationship at "many"(cart_item entity) side
//    @OneToMany(fetch = FetchType.EAGER )
//    @JoinColumn(name="shopping_history_id",referencedColumnName = "id")
//    @JsonIgnore
//    List<CartItem> cartItems = new ArrayList<>();

    public ShoppingHistory(LocalDateTime dateTime, User user) {
        this.dateTime = dateTime;
        this.user = user;
//      this.cartItems = cartItems;
    }
}
