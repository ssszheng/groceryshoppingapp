package com.company.GroceryShoppingApp.shoppingHistory;


import com.company.GroceryShoppingApp.cartItem.CartItem;
import com.company.GroceryShoppingApp.cartItem.CartItemService;
import com.company.GroceryShoppingApp.product.Product;
import com.company.GroceryShoppingApp.product.ProductService;
import com.company.GroceryShoppingApp.user.User;
import com.company.GroceryShoppingApp.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;

//API layer
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(path="shoppingHistory/")
public class ShoppingHistoryController {

    private final ShoppingHistoryService shoppingHistoryService;
    private final CartItemService cartItemService;
    private final UserService userService;
    private final ProductService productService;

    @Autowired
    public ShoppingHistoryController(ShoppingHistoryService shoppingHistoryService, CartItemService cartItemService, UserService userService, ProductService productService) {
        this.shoppingHistoryService = shoppingHistoryService;
        this.cartItemService = cartItemService;
        this.userService = userService;
        this.productService = productService;
    }
    @GetMapping(value = "userId/{id}/all")
    public String getAllCartItemsByUserId(@PathVariable("id") Long id){
        List<ShoppingHistory> histories = shoppingHistoryService.findByUserId(id);
        List<ShoppingHistoryDetail> shdList= new ArrayList<>();
        if (histories.size() !=0 ){
            for (ShoppingHistory history : histories) {
                Long historyId = history.getId();
                LocalDateTime dateTime = history.getDateTime();
                List<CartItem> cartItemList = cartItemService.findCartItemsByShoppingHistoryId(historyId);
                ShoppingHistoryDetail shd = new ShoppingHistoryDetail(historyId,dateTime,cartItemList);
                shdList.add(shd);
            }
        }
        return shdList.toString();
    }
    @GetMapping(value = "{id}")
    public String getShoppingHistoryDetailsById(@PathVariable("id") Long id){
        cartItemService.findCartItemsByShoppingHistoryId(id);
        Optional<ShoppingHistory> shoppingHistory = shoppingHistoryService.findById(id);
        if (shoppingHistory.isPresent()){
            LocalDateTime dateTime = shoppingHistory.get().getDateTime();
            List<CartItem> cartItemList = cartItemService.findCartItemsByShoppingHistoryId(id);
            ShoppingHistoryDetail shd = new ShoppingHistoryDetail(id,dateTime,cartItemList);
            return shd.toString();
        }
        return "{}";

    }
// TODO: can be improved by using OneToMany relationship instead of ManytoOne between cartitem entity and shoppingHistory entity
    @PostMapping(path="userEmail/{email}")
    public void addShoppingHistoryByUserEmail(@RequestBody List<Map<String,Long>> cartItemList,
                                           @PathVariable(value = "email") String email){

        User user= userService.getUserByEmail(email).get();
        //TODO: change dateTime format
        LocalDateTime dateTime = LocalDateTime.of(2022,3,5,11,25);

        ShoppingHistory shoppingHistory = new ShoppingHistory(dateTime,user);
        shoppingHistoryService.addNewShoppingHistory(shoppingHistory);
        //need to get the shopping history from the database with id

       //hoppingHistory shoppingHistoryWithId = shoppingHistoryService.getByUserIdAndDataTime(user.getId(),dateTime);


        //cartItem with quantity, product
        cartItemList.forEach(cartItem -> {
            System.out.println("shoppingHistoryWithId.getId()"+cartItem.get("product"));
            //List<Map<String,Long>>
            //[{"quantity":50, "product":1}]
            Long productId = cartItem.get("product");


            Optional<Product> optionalProduct = productService.getProductsByID(productId);
            Product product = optionalProduct.get();

            Integer quantity = (int)(long)cartItem.get("quantity");

            CartItem newCartItem = new CartItem(quantity,product,shoppingHistory);

            cartItemService.addSingleCartItem(newCartItem);
        });


    }



}
