package com.company.GroceryShoppingApp.shoppingHistory;


import com.company.GroceryShoppingApp.cartItem.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ShoppingHistoryRepository extends JpaRepository<ShoppingHistory,Long>{

    @Query("SELECT s FROM shopping_histories s WHERE s.user.id=?1")
    List<ShoppingHistory> findByUserId(Long id);

    @Query("SELECT s FROM shopping_histories s WHERE s.user.id=?1 and s.dateTime=?2")
    ShoppingHistory findByUserIdAndDateTime(Long id, LocalDateTime dateTime);
}
