package com.company.GroceryShoppingApp.shoppingHistory;

import com.company.GroceryShoppingApp.cartItem.CartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ShoppingHistoryService {
    private final ShoppingHistoryRepository shoppingHistoryRepository;
    private final CartItemRepository cartItemRepository;
    @Autowired
    public ShoppingHistoryService(ShoppingHistoryRepository shoppingHistoryRepository, CartItemRepository cartItemRepository) {
        this.shoppingHistoryRepository = shoppingHistoryRepository;
        this.cartItemRepository = cartItemRepository;
    }

    public List<ShoppingHistory> findByUserId (Long id){
        return shoppingHistoryRepository.findByUserId(id);
    };

    public Optional findById(Long id) {
        return shoppingHistoryRepository.findById(id);
    }


    public void addNewShoppingHistory(ShoppingHistory shoppingHistory) {
        shoppingHistoryRepository.save(shoppingHistory);
    }

    public ShoppingHistory getByUserIdAndDataTime(Long id, LocalDateTime dateTime) {
        return shoppingHistoryRepository.findByUserIdAndDateTime(id,dateTime);
    }
}
