package com.company.GroceryShoppingApp.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

//service layer for business logic
@Service
public class ProductService {
    private final ProductRepository productRepository;
    //dependency injection
    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getProducts() {
        return productRepository.findAll();

    }

    public void addNewProduct(Product product) {
        Optional<Product> productOptional = productRepository
                .findProductByName(product.getName());
        if(productOptional.isPresent()){
            throw new IllegalStateException("product already in the database");
        }
        productRepository.save(product);
    }


    public Optional<Product> getProductsByID(Long id) {
        return productRepository.findProductByID(id);
    }

    public Collection<Product> getProductsByCategory(String category) {
        return productRepository.findProductByCategory(category);
    }
}

