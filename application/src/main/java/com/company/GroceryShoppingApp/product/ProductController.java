package com.company.GroceryShoppingApp.product;

import com.company.GroceryShoppingApp.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

//API layer
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(path="products")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService ProductService) {
        this.productService = ProductService;
    }

    @GetMapping("/all")
    public List<Product> getProducts(){
        return productService.getProducts();
    }

    @GetMapping(value = "/{id}")
    public Optional<Product> getProductsByID(@PathVariable("id") Long id){
        return productService.getProductsByID(id);
    }

    @PostMapping("/all")
    public void addNewProduct(@RequestBody Product product){
        productService.addNewProduct(product);
    }

    //http://localhost:8080/products?category=Dairy
    @GetMapping
    public Collection<Product> getProductsByCategory(@RequestParam("category") String category){
       return productService.getProductsByCategory(category);
    }



}
