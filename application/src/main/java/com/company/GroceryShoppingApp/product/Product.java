package com.company.GroceryShoppingApp.product;

import com.company.GroceryShoppingApp.review.Review;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "products")
@Table(name = "products")
public class Product {


    @Id
    @SequenceGenerator(
            name = "product_sequence",
            sequenceName = "product_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "product_sequence"
    )

    private Long id;
    private String name;
    private Float price;
    //an average rate from all customer review
    // no need to be a column in the database
    @Transient
    private Float rate;

    private String category;
    private String brand;

    @OneToMany(mappedBy = "product")
    List<Review> reviews = new ArrayList<>();

    public Product(){ }

    public Product(Long id, String name, Float price, String category, String brand) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
        this.brand = brand;

    }


    public Product(String name, Float price, String category, String brand) {
        this.name = name;
        this.price = price;
        this.category = category;
        this.brand = brand;
    }

    public Product(Long id, String name, String brand) {
        this.id = id;
        this.name = name;
        this.brand = brand;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public float getRate() {
        long numOfReviews = reviews.stream()
                .filter(review -> review != null)
                .count();
        if (numOfReviews == 0){return -1;}
        else{

        float rate = reviews.stream()
                .filter(review -> review != null)
                .map(review -> review.getRate())
                .reduce(Float.valueOf(0), Float::sum);
        this.rate=rate;

        return rate/numOfReviews;}


    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }
    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
//    @Override
//    public String toString() {
//        return "Product{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", price=" + price +
//                ", rate=" + rate +
//                ", category='" + category + '\'' +
//                ", brand='" + brand + '\'' +
//                ", reviews=" + reviews +
//                '}';
//    }



}
