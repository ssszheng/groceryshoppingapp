package com.company.GroceryShoppingApp.product;

import com.company.GroceryShoppingApp.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

//data access layer
@Repository
public interface ProductRepository
        extends JpaRepository<Product,Long> {
    // this can transform to SQL
    //SELECT * FROM student WHERE email =?
    //or use jpl query
    @Query("SELECT p FROM products p WHERE p.name =?1")
    Optional<Product> findProductByName(String name);

    @Query("SELECT p FROM products p WHERE p.id = :id")
    Optional<Product> findProductByID(@Param("id") Long id);

    @Query("SELECT p FROM products p WHERE p.category = ?1")
    Collection<Product> findProductByCategory(String category);
}
