package com.company.GroceryShoppingApp.login;

import com.company.GroceryShoppingApp.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class LoginService {
    private final UserService userService;

    public String checkLogin(LoginRequest request) {
        boolean userExists = userService.checkLogin(request.getEmail(), request.getPassword());
        if (userExists){return "successfully logged in";}
        else{return "Invalid email or password";}
    }
}
