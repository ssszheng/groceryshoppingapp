package com.company.GroceryShoppingApp.review;

import com.company.GroceryShoppingApp.product.Product;
import com.company.GroceryShoppingApp.user.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;

//a review only has a user and a product
//a user can have many reviews
//a product can have many reviews
@Entity(name = "reviews" )
@Table(name = "reviews")
public class Review {
    @Id
    @SequenceGenerator(
            name = "review_sequence",
            sequenceName = "review_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "review_sequence"
    )
    private Long id;
    private Float rate;
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    @JsonBackReference("user")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference("product")
    @JoinColumn(name = "product_id",referencedColumnName = "id")
    private Product product;

    public Review(){}


    public Review(Long id, User user, Float rate, String comment, Product product ) {
        this.id = id;
        this.user = user;
        this.rate = rate;
        this.comment = comment;
        this.product = product;
    }

    public Review(User user, Float rate, String comment,Product product) {
        this.user = user;
        this.rate = rate;
        this.comment = comment;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }


}
