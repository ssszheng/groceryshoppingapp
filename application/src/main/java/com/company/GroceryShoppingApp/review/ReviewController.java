package com.company.GroceryShoppingApp.review;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//API layer
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(path="reviews/")
public class ReviewController {

    private final ReviewService reviewService;

    @Autowired
    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping("all")
    public List<Review> getReviews(){
        return reviewService.getReviews();
    }
//    @PutMapping("userid/{id}")
//    public Optional<Review> getReviewByUserId(@PathVariable("id") Long id){
//        return reviewService.getReviewByUserId(id);
//    }

//    }
}
