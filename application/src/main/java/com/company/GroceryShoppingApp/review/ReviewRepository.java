package com.company.GroceryShoppingApp.review;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

//data access layer
@Repository
public interface ReviewRepository
        extends JpaRepository<Review,Long> {

    @Query("SELECT r.id FROM reviews r WHERE r.user.id=?1 ")
    List<Long> getReviewIdByUserId(Long userId);


}
