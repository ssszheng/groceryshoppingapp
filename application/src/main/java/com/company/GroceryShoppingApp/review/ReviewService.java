package com.company.GroceryShoppingApp.review;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//service layer for business logic
@Service
public class ReviewService {
    private final ReviewRepository reviewRepository;
    //dependency injection:
    // spring injects the ReviewRepo bin to service
    // thanks to that we can use ReviewRepo methods
    @Autowired
    public ReviewService(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public List<Review> getReviews() {
        return reviewRepository.findAll();

    }

    public Review addNewReview(Review review) {
        return reviewRepository.save(review);
    }

    public Review getReviewById(Long id){
        return reviewRepository.findById(id).orElseThrow(()->
                new ReviewNotFoundException(id));
    }
    public Review deleteReivewById(Long id){
        Review review = getReviewById(id);
        reviewRepository.delete(review);
        return review;
    }



}
