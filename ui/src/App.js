
import './App.css';
import Header from './components/Header.js';
import Navbar from './components/Navbar.js';
import Home from './components/Home';
import { BrowserRouter as Router,Route, Routes } from "react-router-dom";
import Categories from './components/Categories';
import Shoppingcart from './components/Shoppingcart';
import Login from './components/Login';
import ProductDetails from './components/ProductDetails';
import Register from './components/Register';
import AdvancedSearch from './components/AdvancedSearch';
import Logout from './components/Logout';
import CategoryList from './components/CategoryList';
import { useState , createContext, useContext} from 'react';
import Checkout from './components/Checkout.js';
import ShoppingHistory from './components/ShoppingHistory.js';
import ShoppingHistoryDetails from './components/ShoppingHistoryDetails.js';
import {CartItemsContext,CartItemsContextProvider} from './components/Context.js'


// jsx



function App() {

 //const {cartItems,setCartItems}= useContext(CartItemsContext);
 const [cartItems, setCartItems] = useState([
         {
           "id":3,"name":"Apple","price":3.4,"quantity":1,"category":"Fruit","brand":"Gala"
         },
          {
          "id":1,"name":"Milk","price":5.222,"quantity":2,"category":"Dairy","brand":"AMilke"
          }
   ]);
   
  const handleDelete =(id) =>{
      console.log("id"+id);
        const newCartItems = cartItems.filter(item => item.id !== id);
        setCartItems(newCartItems);
    }

    const handleAdd=(product) =>{
      //check if the product in the cart already and get that product if exists
      console.log(cartItems);
      const exist = cartItems.find((item) => item.id === product.id);
      if (exist) {
        setCartItems(
          cartItems.map((item) =>
            item.id === product.id ? { ...exist, quantity: exist.quantity + 1 } : item
          )
        );
      } else {
        setCartItems([...cartItems, { ...product, quantity:1}]);
      }
    };
  
  
    
    

  return (
  
    <Router>
     
      <div className="App"> 
      <CartItemsContextProvider>
      <Header header='Grocery Shopping'/>
      <div><Navbar /></div>
      <div>Cart items: {cartItems&& cartItems.map((ci,i)=>(<p key={i}>{ci.id} qty: {ci.quantity}</p>))}</div>
      <Routes>
        <Route path="/" element ={<Home/>} />
        <Route path="/categories" element ={<Categories/>} />
        <Route path="/shoppingcart" element ={<Shoppingcart cartItems={cartItems} 
                                                            handleDelete={handleDelete}/>} />
        <Route path="/login" element ={<Login/>} />
        <Route path="/logout" element ={<Logout/>} />
        <Route path="/register" element ={<Register/>} />
        <Route path="/products/:id" element ={<ProductDetails addToCart = {handleAdd}/>} />
        <Route path="/products" element ={<CategoryList />} />
        <Route path="/advancedsearch" element ={<AdvancedSearch/>} />
        <Route path="/checkout" element ={<Checkout/>} />
        <Route path="/shoppingHistory/userId/:id/all" element={<ShoppingHistory/>}/>
        <Route path="/shoppingHistory/:id" element={<ShoppingHistoryDetails/>}/>
      </Routes> 
      </CartItemsContextProvider>
      </div>
    </Router>
 
  );
}

export default App;