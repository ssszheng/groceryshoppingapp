import {LOGIN_REQUEST, LOGOUT_REQUEST, SUCCESS, FAILURE} from './authTypes';
import axios from 'axios';


const loginURL = "http://localhost:8080/login";

export const authenticateUser = (email, password) => 
    async (dispatch) => {
        dispatch(loginRequest());
        try {
        const response = await axios.post(loginURL, {
            email: email,
            password: password,
        });
        dispatch(success(true));
        localStorage.setItem("jwtToken", response.data.token);
        dispatch(success({ username: response.data.name, isLoggedIn: true }));
        return Promise.resolve(response.data);
        } catch (error) {
        dispatch(failure());
        return Promise.reject(error);
        }
    };
       

const loginRequest = () => {
    return {
        type: LOGIN_REQUEST
    };
};

export const logoutUser =() =>{
    return dispatch =>{
        dispatch(logoutRequest());
        localStorage.removeItem("jwtToken");
        dispatch(success({ username: "", isLoggedIn: false }));
    };

};
const logoutRequest = () => {
    return {
        type: LOGOUT_REQUEST
    };
};

const success = isLoggedIn => {
    return {
        type: SUCCESS,
        payload: isLoggedIn
    };
};

const failure = () => {
    return {
        type: FAILURE,
        payload: false
    };
};

