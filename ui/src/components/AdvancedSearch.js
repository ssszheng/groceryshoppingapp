import { useState } from "react"

const AdvancedSearch = () => {
    const [name,setName] = useState('');
    //select field, the initial value is one of the option
    //auto selected
    const [category,setCategory] = useState('NotSelected');
    return (
        <div className="advanced-search">
            <h2>Advanced Search</h2>
            <form>
                <label>Product name</label>
                <input 
                type="text"
                value = {name}
                onChange = {(e)=> setName(e.target.value)}/>

                <p>Product name: {name}</p>

                <label>Category</label>
                <select
                    value ={category}
                    onChange={(e) => setCategory(e.target.value)}>   
                    <option value="NotSelected"></option>
                    <option value="Vegetables">Vegetables</option>
                    <option value="Fruits">Fruits</option>
                    <option value="Dairy">Dairy</option>
                </select>
                <p>Category: {category}</p>
                <button>Search</button>
                
            </form>
        </div>
    )
}

export default AdvancedSearch
