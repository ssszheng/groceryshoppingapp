
import ProductsList from "./ProductsList";
import useFetch from "./useFetch.js";
import {useSearchParams } from "react-router-dom";


const urlPath = "http://localhost:8080"

const CategoryList =()=>{
    
    let [searchParams, setSearchParams] = useSearchParams();
    let category =  searchParams.get("category");

    const {data:products,isPending,error} = useFetch(urlPath+"/products?category="+category);
    var jsonProducts = JSON.parse(products);
    return (
        <div>
        <h1>Category: {category}</h1>
        { error && <div> {error}</div> }
        {isPending && <div>Loading...</div>}
        {products && <ProductsList products ={jsonProducts} 
        title="" hiddenDelete = {true}/>}
        </div>
    ) 
}

export default CategoryList