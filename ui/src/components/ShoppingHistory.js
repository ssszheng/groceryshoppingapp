import { useParams } from "react-router";
import useFetch from "./useFetch";
import { useState } from "react";
import { Link } from "react-router-dom";

const urlPath = "http://localhost:8080/shoppingHistory/userId/"

const ShoppingHistory = () => {

     //to grab the parameter from the route
     const {id} = useParams();
     const {data,error,isPending} = useFetch(urlPath+id+"/all");
     const shoppingHistory = JSON.parse(data);


    return (
        <div>
            <h1>All Shopping Histories </h1>
            <p>raw: {shoppingHistory&&JSON.stringify(shoppingHistory)}</p>
            {isPending && <div>Loading...</div>}
            {error && <div>{error}</div>}
            <div className="Shopping-history">
                {shoppingHistory&& shoppingHistory.map((sh)=>(
                <div key={sh.id} > 
                    <Link to={`/shoppingHistory/${sh.id}`}>
                        shoppinghistory ID: {sh.id} &nbsp;&nbsp;
                        DateTime: {sh.dateTime}</Link>
            </div>))}
        </div>

        

          
        </div>
    );
};

export default ShoppingHistory;