
import ProductsList from "./ProductsList"
import useFetch from "./useFetch.js"
import authToken from "../utils/authToken";
import {useLocation} from 'react-router-dom';

const urlPath = "http://localhost:8080"

const Home = () => {
    if (localStorage.jwtToken) {
        authToken(localStorage.jwtToken);
      }
    
    const location = useLocation();
    const message = location.state? location.state: null;

    const {data:products,isPending,error} = useFetch(urlPath+"/products/all");
    var jsonProducts = JSON.parse(products);
    const {data:test,isPending:is,error:err} = useFetch(urlPath+"/users/all");
    




    return (
        <div>
         <h1>Welcome to Grocery Shopping App</h1>
         <p>message:{message}</p>
         { error && <div> {error}</div> }
         {isPending && <div>Loading...</div>}
         {products && <ProductsList products ={jsonProducts} 
         title="Today's recommended" hiddenDelete = {true}/>}
         <p>Test: {test}</p>
         
         {/* <ProductsList products ={products
            .filter((product)=> product.rate === 5.0)} 
            title="Today's on sale"/>     */}
        </div>
    )
}

export default Home
