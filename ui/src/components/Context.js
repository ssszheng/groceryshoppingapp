
import  React,{ useState , createContext} from 'react';

//this file stores different context
export const CartItemsContext = createContext({});

const CartItemsContextProvider =({ children }) =>{
    const [cartItems, setCartItems] = useState([
        {
          "id":3,"name":"Apple","price":3.4,"quantity":1,"category":"Fruit","brand":"Gala"
         },
         {
          "id":1,"name":"Milk","price":5.222,"quantity":2,"category":"Dairy","brand":"AMilke"
          }
  ]);
    return (
        <CartItemsContext.Provider value = {{cartItems,setCartItems}}>
            { children }
        </CartItemsContext.Provider>
    );
};
export {CartItemsContextProvider};

