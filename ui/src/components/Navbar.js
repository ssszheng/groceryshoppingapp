import {Link} from 'react-router-dom';
const Navbar = () => {
    return (
        <div style={styling}>
            <Link to="/">Home</Link>
            <Link to="/categories">Categories</Link>
            <Link to="/shoppingcart">Shopping cart</Link>
        </div>  
    )
}
const styling = {
    backgroundColor:'LightGreen',
    padding: "10px",
    border: "10px",
    fontSize:'20px',
    fontFamily:'Courier New',
    fontWeight:'bold',
    textalign: 'center',
    flexDirection: "row",
    display: 'flex',
    justifyContent:"space-around",
}
export default Navbar
