import { Link } from "react-router-dom";

const ProductsList = (props) => {
    const products = props.products;
    const title = props.title;
    const handleDelete = props.handleDelete;
    const hiddenDelete = props.hiddenDelete;
    //regarding the handleDelete function:
    //product state is initialized in other componennts (e.g. shoppingCart)
    //should not directly edit the props inside the this (productList) component
    //thus we can invoke the handleDelete function which is defined at App level 
    return (
        <div>
            <h1>{title}</h1>
            {products.map((product) => (
                <div className = "All-products" key={product.id} >
                    <Link to={`/products/${product.id}`}> 
                    <h2>{ product.name }</h2>
                    </Link>
                    <p>{ product.price}</p>
                    <p>{ product.category}</p>
                    <button hidden={hiddenDelete} onClick={()=> handleDelete(product.id)}>Delete</button>
                </div>))}
            
        </div>
    )
}

export default ProductsList
