import { Link } from 'react-router-dom';
import '../App.js';
import logo from '../logo.png';
import { logoutUser } from "../services/index";
import { useDispatch, useSelector } from "react-redux";

const Header = ({header}) => {
    const auth = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    const logout = () => {
        dispatch(logoutUser());
      };


    const guestLinks = ( 
    <><Link to="/login"> Login</Link><Link to="/register"> Register</Link></>
    );

    const userLinks = ( 
        <><Link to={"/logout"} onClick={logout}> Logout</Link></>
        );



    return (
      
        <header className='App-header'>
            <h1>auth.isLoggedIn: {auth.isLoggedIn}</h1>
            
            <div style={{flexDirection: "row", display: 'inline-flex'}}>
                <div><img src={logo} className ="App-logo" alt = "logo" /></div>
                    <div><h1>{header}</h1></div>
                    <div style={{flexDirection: "column", display: 'inline-flex',justifyContent:'flex-start',alignItems: 'flex-end'}}>
                    {auth.isLoggedIn ? userLinks : guestLinks}
                </div>
            </div>           
        </header> 
          
        
    )
}

export default Header
