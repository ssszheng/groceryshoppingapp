import { useParams } from "react-router";
import useFetch from "./useFetch";
import { useState } from "react";
import { Link } from "react-router-dom";

const urlPath = "http://localhost:8080/shoppingHistory/"

const ShoppingHistoryDetails = () => {
    //to grab the parameter from the route
    const {id} = useParams();
    const {data,error,isPending} = useFetch(urlPath+id);
    const shoppingHistory = JSON.parse(data);
    return (
        <div>
            <h1>Shopping History Details for User Id {id}</h1>
            <p>raw: {shoppingHistory&&JSON.stringify(shoppingHistory)}</p>
            {isPending && <div>Loading...</div>}
            {error && <div>{error}</div>}
            <div className="Shopping-history-details">
               <div>Id: {shoppingHistory&&shoppingHistory.id}</div> 
               <div>Date: {shoppingHistory&&shoppingHistory.dateTime}</div>
               <div>Cart Items:</div>
               {shoppingHistory && shoppingHistory.cartItems.map((sh)=>(
                   <div key={sh.product[0]}>Quantity: {sh.quantity}  <Link to={`/products/${sh.product[0]}`}>Product Name: {sh.product[1]} Brand: {sh.product[2]} </Link> </div>
               ))}

            </div>

            
        </div>
    );
};

export default ShoppingHistoryDetails;