import { useParams } from "react-router";
import useFetch from "./useFetch";
import { useState } from "react";

const urlPath = "http://localhost:8080/products/"
const ProductDetails = ({addToCart}) => {
    //to grab the parameter from the route
    const {id} = useParams();
    const {data:productText,error,isPending} = useFetch(urlPath+id);
    const product = JSON.parse(productText);

    const [user,setUser] = useState('');
    const [rate,setRate] = useState('');
    const [reply,setReply] = useState('');

    const itemToCart = product && {
        "id":product.id,"name":product.name,"price":product.price,"category":product.category,"brand":product.brand
       }

    const handleSubmitReview =(e)=>{
         //prevent refresh the page after click the submit button
         e.preventDefault();
         //review object 
         const newReview = {user,rate,reply};
         console.log("user: ",user);
         fetch(urlPath+ product.id,{
                   
        }).then(() => {

        })
    }
   
    return (
        <div className="Product-details">
           <h2>Product details - id: {id}</h2>
           {isPending && <div>Loading...</div>}
           {error && <div>{error}</div>}
           <p>raw product message: {product && JSON.stringify(product)}</p>
           {product && (
           <div>
               <div>
               <p>Prodct name: {product.name} </p>
               <p>Price: ${product.price} </p>
               <p>Brand: {product.brand} </p>
               <p>Category: {product.category} </p>
               <p>Rate: { product.rate!== -1.0 ?  product.rate:"No review yet" } </p>
               <p>Review:</p> {product.reviews.map((review) => (
                   <p key={product.id}>UserID {review.id}'s review: {review.comment}</p>
                   ))}
                
                <button onClick={() => addToCart(itemToCart)}>Add To Cart</button>
                </div>
               <div>
               <form onSubmit={handleSubmitReview} className="Review-form">
                   <label>Rate</label>
                   <input 
                    type='number'
                    step="0.5"
                    min='0'
                    max='5'
                    value={rate}
                    onChange={(e) => setRate(e.target.value)}
                />
                   
                   <label>Reply</label>
                   <textarea 
                        rows={8}
                        value = {reply}
                        placeholder="Write something.." 
                        onChange={(e)=> setReply(e.target.value)}/>
                <button>Add</button>
               </form>
               </div>
            </div>
           )}</div>
    )
};

export default ProductDetails;
