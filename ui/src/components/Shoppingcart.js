import { useState, useContext  } from 'react';
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector} from "react-redux";
import {CartItemsContext} from "../components/Context.js";
const Shoppingcart = ({handleDelete}) => {
    const auth = useSelector((state) => state.auth);
    let navigate = useNavigate();
   //pass handleDelete function as a prop
   //and use setProduct to update the state at the App level
   const {cartItems,setCartItems}= useContext(CartItemsContext);


   const toCheckout = () => {navigate('/checkout',{state:{price:totalPrice}})}
   const loginAlert = (<button onClick={()=> alert("Need to login first!")}> Checkout </button>);
   const itemsPrice = cartItems.reduce((previous, current) => previous + current.quantity * current.price, 0);
   const taxPrice = itemsPrice * 0.10;
   const shippingPrice = itemsPrice > 60 ? 0 : 10;
   const totalPrice = (itemsPrice + taxPrice + shippingPrice).toFixed(2);
 
  
    return (
        <div>
            <h1>My Shopping cart</h1>
            {cartItems.length === 0 && <div>Cart is empty</div>}
            {cartItems.map((item) => (
            <div key={item.id} className='Shopping-cart'><Link  to={`/products/${item.id}`}> 
                    <h2 >{item.name}</h2></Link> 
                    <p> quantity: {item.quantity} X price: ${item.price} </p>
                    <p></p>
                    <button onClick={()=>handleDelete(item.id)}>Delete</button>
            </div> ))}
            <div className='Shopping-cart-summary'>
            <div>Items price: $ {itemsPrice.toFixed(2)}</div>
            <div>Tax price: $ {taxPrice.toFixed(2) }(10%)</div>
            <div>Shipping fee: $ {shippingPrice}</div>
            {totalPrice < 60 && <div>Free shipping when total price &gt; $60 </div>}
            <div>Total price: $ {totalPrice}</div>
            <h1>auth:{auth.isLoggedIn}</h1>
            {auth.isLoggedIn ? <button onClick={()=>toCheckout()}> Checkout </button> : loginAlert}
            </div>
        </div>
    )
}

export default Shoppingcart
