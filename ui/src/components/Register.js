import { useState } from "react";
import {useNavigate} from "react-router-dom";

const Register = () => {
    const [name,setName]= useState('');
    const [email,setEmail]= useState('');
    const [password,setPassword]= useState('');
    const [isPending,setIsPending] = useState(false);
    const navigate = useNavigate();
    const urlPath ='http://localhost:8080/register'



    const handleSubmit = (e) =>{
        //prevent refresh the page after click the submit button
        e.preventDefault();
        //username object
        const user = {name,email,password};
        console.log(user);
        
        setIsPending(true);

        //fetch request use fetch api
        fetch(urlPath,{
            method :'POST',
            // credentials: 'include',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Access-Control-Allow-Credentials': true
            },
            body:JSON.stringify(user)
            
        }).then(()=>{
            console.log("New user added");
            console.log(JSON.stringify(user));
            setIsPending(false);
            navigate('/');
        })

    }
    return (
        <div className = "register">
        <h2>Register</h2>
        <form onSubmit = {handleSubmit}>
            <label>User Name</label>
            <input 
                type="text"
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
            />
            
            <label>Email</label>
            <input 
                type="text"
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
            />
            <label>Password</label>
            <input 
                type="text"
                required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
            />
            {! isPending && <button>Submit</button>}
            { isPending && <button disabled>Submiting...</button>}
        </form>
        </div>
    )
}

export default Register
