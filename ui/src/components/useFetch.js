import { useState,useEffect } from "react";

const useFetch =(url) => {

    const [data, setData] = useState(null);
    const [isPending, setIsPending] = useState(true);
    const [error,setError] =useState(null);



    useEffect(()=>{
        
        const abortCont = new AbortController();

        //adding an argument to fetch,
        // and use signal property 
        //and set it equal to abort constant abortCont
        //assoicated the abort controller with the fetch 
         //to stoip the fetch
        fetch(url, {signal: abortCont.signal})
        .then(res => {
            if (!res.ok){
                throw Error("could not fetch the data for that resources");
            }
            console.log(res);
            return res.text();
        })
        .then(data => {
            setData(data);
            setIsPending(false);
            setError(null);
        })
        .catch(err =>{
            //if it'a an abort error, 
            //don't want to update the state
            if(err.name === 'AbortError'){
                console.log("fetch aborted")
            }else{ setIsPending(false);
            setError(err.message);
        }   
        })
        //stop the fetch in the background
        //abort the fecth
        return () => abortCont.abort();
    },[url])
    //place the url as a dependency to useEffect
    //so that whenever the url changes,
    // the function is going to re-run to get the data from the endpoint
   //return an object with three properties
   console.log(data);
   return {data,isPending,error};
}


export default useFetch;