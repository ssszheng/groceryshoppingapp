import {useLocation,Link,useNavigate} from 'react-router-dom';
import { useDispatch, useSelector} from "react-redux";
import authToken from "../utils/authToken";
import { useState, createContext, useContext } from "react";
import {CartItemsContext} from "../components/Context.js";

const Checkout = () => {
    // const urlPath ='http://localhost:8080/checkout'
   

    const location = useLocation();
    // const totalPrice = location.state ? location.state.price :0;
    const totalPrice = location.state.price;
    const [isPending,setIsPending] = useState(false);
    let navigate = useNavigate();
    const auth = useSelector((state) => state.auth);
    const {cartItems,setCartItems}= useContext(CartItemsContext);
    const urlPath ='http://localhost:8080/shoppingHistory/userEmail/'+ auth.username;
    // List<Map<String,Long>> 
    //e.g. [{"quantity":50, "product":1}]
    const cartItemList = [];
    cartItems.map((ci)=>(cartItemList.push({"quantity":ci.quantity, "product":ci.id})));
    
    


    const handleSubmit = (e) =>{
        //prevent refresh the page after click the submit button
        e.preventDefault();
        //username object
        
        // console.log(JSON.stringify(cartItemList));
        
        setIsPending(true);

        //fetch request use fetch api
        fetch(urlPath,{
            method :'POST',
            // credentials: 'include',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Access-Control-Allow-Credentials': true
            },
            body:JSON.stringify(cartItemList)
            
        }).then(()=>{
            console.log("New shopping submission");
            console.log(JSON.stringify(cartItemList));
            setIsPending(false);
            navigate('/',{state:"Shopping sucessfully"});
        })

    }

    const loggedIn = (<><h1>Checkout</h1>
        <p>auth: {auth.username}</p>
            <div>Total Price: {totalPrice}</div>

            <div>Online banking: </div>
            <div>To do: online banking or emailing bill api </div>
            <button onClick={handleSubmit}>Confirm (assume success payment)</button></>);
    const notLoggedIn = (<><div>Please <Link to="/login"> Login</Link> or <Link to="/register"> Register</Link> first</div></>);

    
    return (
        <div>
            {auth.isLoggedIn? loggedIn: notLoggedIn}
            <div>Cart items!!!: {cartItems&& cartItems.map((ci,i)=>(<p key={i}>{ci.id} qty: {ci.quantity}</p>))}</div>
         


            
        </div>
    );
};

export default Checkout;