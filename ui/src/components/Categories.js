import useFetch from "./useFetch.js";
import { Link } from "react-router-dom";

const urlPath = "http://localhost:8080"

const Categories = ({message}) => {
    const {data:products,isPending,error} = useFetch(urlPath + "/products/all");
    var jsonProducts = JSON.parse(products);
    const categories = [];

    if (jsonProducts!= null) jsonProducts.map((product)=>(categories.includes(product.category)) ? null : categories.push(product.category));

    return (
        <div>
            <h1>Categories</h1>
            <p>message:{message}</p>
            { error && <div> error:{error}</div> }
            {isPending && <div>Loading...</div>}
            {categories.map((cate,i) => (
                <Link to={`/products?category=${cate}`} key={i}>
                     <h2>{cate}</h2>
                </Link>
            ))}
        </div>
                
    )
}

export default Categories
