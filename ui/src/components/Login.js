import React, { useState } from 'react';
import {authenticateUser} from '../services/index';
import {useNavigate} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import authToken from "../utils/authToken";


     
const Login =(props)=> {
    const [error,setError]= useState('');
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const initialState = {
        email: "",
        password: "",
      };

    const [user, setUser] = useState(initialState);

    const credentialChange = (event) => {
        const { name, value } = event.target;
        setUser({ ...user, [name]: value });
      };

    const resetLoginForm = () => {
        setUser(initialState);
        setError("");
      };     
    const auth = useSelector((state) => state.auth);
    
    const validateUser = (event) =>{
        event.preventDefault();
        dispatch(authenticateUser(user.email,user.password))
        .then((resp) => {
            console.log("repsonse: "+resp);
            //ToDO: navigate to the previous page 
            navigate('/');
        })
        .catch((err) => {
            console.log(err.message);
            resetLoginForm();
            setError(err.message);
        });
  };

        return (
            <div className = "login">
            <h2>Login</h2>
            { error && (<p> Error: {error} </p>)}
            { user.email && (<p> {user.email} </p>)}
            { user.password && (<p> {user.password} </p>)}
            <form>
                <label>Email</label>
                <input 
                    type="text" 
                    required
                    name="email"
                    value={user.email}
                    placeholder="Enter Email"
                    onChange={credentialChange}
                />
                <label>Password</label>
                <input 
                    type="text"
                    required
                    name="password"
                    value={user.password}
                    placeholder="Enter password"
                    onChange={credentialChange}
                />
                <button
                    onClick={validateUser}
                    disabled={user.email.length === 0 || user.password.length === 0}
                    > Login
                </button>
                <button
                    onClick={resetLoginForm}
                    disabled={user.email.length === 0 && user.password.length === 0 && error.length === 0}
                    >Reset</button>
            </form>
            </div>
        );
    }

export default Login;
